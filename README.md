# Collecting_data

This file contains the results obtained in the user study made in our research work on best-view-selection.

**The results are currently not visible, as we are awaiting acceptance of our research paper.**
#
# models
Our 3D model database consists of 44 regular models. 
We have acquired 26 models from [Visual Attention for Rendered 3D Shapes](https://perso.liris.cnrs.fr/guillaume.lavoue/data/saliency/), and the other 18 are freely available on the web.  

Pre-processing has been applied to these models. 
All models are :
* non-textured,
* positioned with the Y axis running from bottom to top: gravitational upwards (when it is meaningful), 
* resized between -1 and 1,
* centered at the origin.
#
# json_files
## **Context:**
There are 203 json files corresponding to the results of each of the 203 users recruited using the platform [Prolific](https://www.prolific.co/).

Each user studies **10 different 3D models**, and must choose **3 viewpoints** which he feels satisfy the main question of the study. 
The 10 models are **randomly** selected from the dataset and displayed according to a **random viewpoint**.

Once this study is complete, each user must explain, for a subset of **5 studied 3D models**, why he made them. 
Keywords are suggested to help them justify their choices : *Side view, Front view, Global view, Eyes contact, Pleasant, Recognizable, 3/4 view*.
A free text field is also available for users to enter their reasons.

## **Camera positions:**
<img src="axes.png"  width="300" height="300">

The user can switch between **26 different camera positions** distributed over a sphere centered on the mesh, as illustrated in the figure.

Camera positions $C_{i,j}$ are defined by two angles ($\alpha_j, \beta_i$):

$\forall i \in [0,7], \ \forall j \in [0,4]$

$$
\begin{equation*}
C_{i,j} = 
\left\{
    \begin{array}{ll}
        x_{ij} = R \cos(\alpha_j)\cos(\beta_i)\\
        y_{ij} = R \sin(\alpha_j)\\
        z_{ij} = R \cos(\alpha_j)\sin(\beta_i)
    \end{array}
\right.
\end{equation*}
$$

with $R=2.2$, $\beta_i = i.\pi/4$ and $\alpha_j = (2-j).\pi/4$.

When $|\alpha_j|=\pi/2 \Rightarrow \cos(\alpha_j) = 0,$ we obtain the same camera position eight times. Only one of them is kept because we always have exactly the same view. 

## **Organizing json files:**
    "Participant"
        |__ "Age"
        |__ "Gender"

    "Tasks"
        |__ "Task_1"
                |__ "obj_name": name of the 3D model that was viewed during task n°1
                |__ "random_initial_position": random intial position of viewed 3D model
                |__ "choices"
                    |__ "choice_1": first selected point of view defined by its two angles (α,β)
                    |__ "choice_2": second selected point of view defined by its two angles (α,β)
                    |__ "choice_3": third selected point of view defined by its two angles (α,β)
        |__ "Task_2"
                |__ ...
        .
        .
        |__ "Task_10"
                |__ ...

    "Analyses"
        |__ "Analyse_1"
                |__ "obj_name": name of the 3D model that was viewed during analyse n°1
                |__ "keywords": ordered list of selected keywords
        |__ "Analyse_2"
                |__ ...
        .
        .
        |__ "Analyse_5"
                |__ ...

